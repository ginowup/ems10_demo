def do_twice(f):
    f()
    f()
    
def print_spam():
    print('spam')

do_twice(print_spam)

# Even aan Daniël vragen waarom dit niet werkt.
# Werkt niet omdat je de functie print_spam laat uitvoeren binnen de aanroep van do_twice. 
# Het probeert nu de return waarde van print_spam() uit te voeren. Het is nu gefixd.
